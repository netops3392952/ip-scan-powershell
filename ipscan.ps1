$host.ui.RawUI.WindowTitle = "IP Scan by @velizjulian13"
$currentTime = Get-DATE -format "dd-MM-yyyy_HHmmss"
$Net = [Microsoft.VisualBasic.Interaction]::InputBox("Please enter first three octets of IP address:", "IP Scan", "")
$ips = @()
for ($i=1; $i -le 254; $i++){$ips += @("$Net.$i")}
$ips | ForEach-Object -Parallel {if (Test-NetConnection -RemoteAddress $_ -InformationLevel Quiet -WarningAction SilentlyContinue) {"IP $_ is UP" }} | Out-File -FilePath .\$Es.1-254_$currentTime.txt